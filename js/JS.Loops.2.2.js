// JavaScript Document

//Skriv en funktion isPrime (p), der returnerer true, hvis p er prime og falsk ellers.

function isPrime(num) {
  for(var i = 2; i < num; i++)
    if(num % i === 0) return false;
  return num > 1;
}

var checkPrime = function(num) {
    console.log("Er dette tal et primtal? " + num);
    console.log(isPrime(num));
};

checkPrime(5);