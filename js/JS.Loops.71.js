// JavaScript Document

/*Tegn en tabel, der viser værdien af variablerne i og n under udførelsen af loop. 
Tabellen skal indeholde en kolonne for hver variabel og en linje for hver iteration. Du kan narre programmet til at gøre det for dig.
Hvad er output fra dette program?
Kan du bevise/rationalisere, at denne loop slutter for en positiv værdi af n?*/

'use strict';
/* Downey: thinkjava, ex 71 */
const loop = function(n) {
    let i = n;
    while (i > 1) {
        console.log(i);
        if (i % 2 == 0) {
            i = i / 2;
        } else {
            i = i + 1;
        }
    }
}

loop(10);

/*
var table = document.getElementById("mytab1");
for (var i = 0, row; row = table.rows[i]; i++) {
   //iterate through rows
   //rows would be accessed using the "row" variable assigned in the for loop
   for (var j = 0, col; col = row.cells[j]; j++) {
     //iterate through columns
     //columns would be accessed using the "col" variable assigned in the for loop
   }  
}
Fra hjemmeside: https://stackoverflow.com/questions/3065342/how-do-i-iterate-through-table-rows-and-cells-in-javascript

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration
*/