// JavaScript Document

/*
Et andet matematisk begreb, Fibonacci -tal [8], vigtigt i forskellige sammenhænge f.eks. Tæt forbundet med beregning af det gyldne snit. Definitionen af det n-th Fibonacci-tal Fn:

F0 = 0; F1 = 1;

og:

Fn = Fn-1 + Fn-2

Skriv en rekursiv funktion fibo (n), der beregner det n-th Fibonacci-tal. Dokumenter det med et par testkasser. Gem funktionen i myRecurseLib.js.
*/


function fibonacci(num) {
    if(num < 2) {
        return num;
    }
    else {
        return fibonacci(num-1) + fibonacci(num - 2);
    }
}

// take nth term input from the user
const nTerms = prompt('Opgave: JS.Recursion.1 - Enter the number of terms: ');

if(nTerms <=0) {
    console.log(10);
}
else {
    for(let i = 0; i < nTerms; i++) {
        console.log(fibonacci(i));
    }
}