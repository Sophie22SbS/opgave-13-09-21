// JavaScript Document

/*
Skriv et program, der bruger console.log til at udskrive alle numre fra 1 til 100, med to undtagelser. For tal, der kan deles med 3, udskrives "Fizz" i stedet for tallet, og for tal, der kan deles med 5 (og ikke 3), udskrives "Buzz" i stedet. Gem som fizzbuzz.js.

Når du får det til at fungere, skal du ændre dit program til at udskrive "FizzBuzz" for tal, der kan deles med både 3 og 5 (og stadig printe "Fizz" eller "Buzz" for tal, der kun kan deles med et af dem). Gem som fizzbuzzBetter.js. (Dette er faktisk et spørgsmål om jobsamtale, der er blevet hævdet at fjerne en betydelig procentdel af programmørkandidater. Så hvis du løste det, steg din arbejdsmarkedsværdi bare.)
*/

for(let i=1;i<=100;i++)
{
    // if iteration number divisible to 3, block works.
    if(i%3==0)
    {
        document.write(`${i} Fizz<br>`); 
    }
}

for(let i=1;i<=100;i++)
{
    // if iteration number divisible to 3, block works.
    if(i%5==0)
    {
        document.write(`${i} Buzz<br>`); 
    }
}

for(let i=1;i<=100;i++)
{
    // if iteration number divisible to 3, block works.
    if(i%3==0&&i%5==0)
    {
        document.write(`${i} FizzBuzz<br>`); 
    }
}