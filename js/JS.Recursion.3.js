// JavaScript Document

/*
En palindrom er et ord, tal, sætning eller anden sekvens af tegn, der læser det samme baglæns som fremad, f.eks. Fru eller racerbil. Der er også numeriske palindromer, herunder dato/tidsstempler med korte cifre 11/11/11 11:11 og lange cifre 02/02/2020. Sætningslængde palindromer ignorerer store bogstaver, tegnsætning og ordgrænser. [9]

Skriv en rekursiv funktion isPalindrome (s), der kontrollerer strengen s for at være en palindrome. Det skal returnere sandt, hvis s er en palindrom, og falsk ellers.
Tip: Tag et kig på strengens funktioner i din MDN -reference. Se på metoden substraat, understring og snitstreng.
*/

var firstCharacter = function(str) {
    return str.slice(0, 1);
};

var lastCharacter = function(str) {
    return str.slice(-1);
};

var middleCharacters = function(str) {
    return str.slice(1, -1);
};

var isPalindrome = function(str) {
    if (str.length < 2) {
        return true;
    }

    if (firstCharacter(str) == lastCharacter(str)) {
        return isPalindrome(middleCharacters(str));
    }

    return false;
};
var checkPalindrome = function(str) {
    console.log("Er dette ord en palindrom? " + str);
    console.log(isPalindrome(str));
};


checkPalindrome("a");
checkPalindrome("matom");
checkPalindrome("rotor");