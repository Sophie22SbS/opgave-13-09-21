// JavaScript Document

/*
//Baseret på dagens if-sætninger og modelløsninger skriver et JavaScript-program, 
//der tæller antallet af hvert muligt resultat ved at rulle en terning n gange. 
//n skal være et tal, der er indtastet af brugeren.


"use strict";


const roll = function (n) {
	return Math.floor(Math.random()*n+1);
}


let ones = 0;
let twos = 0;
let threes = 0;
let fours = 0;
let fives = 0;
let sixes = 0;
let res = roll(6); 


if (res === 1) {
    ones++;
}
else if (res === 2){
	 twos++;
}
else if (res === 3){
	threes++;
}
else if (res === 4){
	 fours++;
}
else if (res === 5){
	 fives++;
}
else{
	sixes++;
}


console.log(res);
*/






/*
n er de antal gange brugeren har slået med terningen.
Brugeren skal kunne indtaste, hvor mange gange de vil slå med terningen.
Brugeren skal kunne se resultatet for hvert kast.
Problem: De kan ikke indtaste deres antal terningkast.
Hjemmeside: https://stackoverflow.com/questions/56918656/multiple-outputs-using-math-random-function
*/

const $ = function (foo) {
	return document.getElementById(foo);
};

//let input = $("input");
//input = multRandEmotes; 

const randomemote = [
	`:1:`,
	`:2:`,
	`:3:`,
	`:4:`,
	`:5:`,
	`:6:`
];
const randEmote = () => randomemote[Math.floor(Math.random()*randomemote.length)];
const multRandEmotes = count => Array.from({ length: count }, randEmote).join('');
const strToSend = `
  ${multRandEmotes(5)}
  `;
console.log(strToSend);






