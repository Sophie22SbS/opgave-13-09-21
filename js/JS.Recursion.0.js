// JavaScript Document

/*Det matematiske koncept, factorial [6], er vigtigt i sandsynlighed og statistik. Definitionen af n !:

n! = n * (n -1) * (n - 2) * ... * 2 * 1

Der er en smukt kort og elegant rekursiv definition:

n! = n * (n - 1)!

Skriv en rekursiv funktionsfakta (n), der beregner faktorialet for et givet naturligt [7] tal n. Dokumenter det med et par testkasser. Placer funktionen i myRecurseLib.js.*/

//Hjemmeside: https://stackoverflow.com/questions/43426769/javascript-factorial-with-recursion

var fact=5;
function calfact(num)
{
 if(num!=1)
  {
   fact=fact*(num-1);
   num=num-1;
   return calfact(num); // ***
  }
 else
  {
   return fact;
  }
}
function factorial(num) {
    if (num < 0) {
        throw new Error("num must not be negative");
    }
    if (num <= 1) {
        // Both 1! and 0! are defined as 1
        return 1;
    }
    return num * factorial(num - 1);
}
console.log(factorial(5)); // 120

var fact2=10;
function calfact2(num2)
{
 if(num2!=1)
  {
   fact2=fact2*(num2-1);
   num2=num2-1;
   return calfact2(num2); // ***
  }
 else
  {
   return fact2;
  }
}
function factorial2(num2) {
    if (num2 < 0) {
        throw new Error("num must not be negative");
    }
    if (num2 <= 1) {
        return 1;
    }
    return num2 * factorial2(num2 - 1);
}
console.log(factorial2(10)); 

var fact3=1;
function calfact3(num3)
{
 if(num3!=1)
  {
   fact3=fact3*(num3-1);
   num3=num3-1;
   return calfact3(num3); // ***
  }
 else
  {
   return fact3;
  }
}
function factorial3(num3) {
    if (num3 < 0) {
        throw new Error("num must not be negative");
    }
    if (num3 <= 1) {
        return 1;
    }
    return num3 * factorial3(num3 - 1);
}
console.log(factorial3(1)); 

var fact4=-7;
function calfact4(num4)
{
 if(num4!=1)
  {
   fact4=fact4*(num4-1);
   num4=num4-1;
   return calfact4(num4); // ***
  }
 else
  {
   return fact4;
  }
}
function factorial4(num4) {
    if (num4 < 0) {
        throw new Error("num must not be negative");
    }
    if (num4 <= 1) {
        return 1;
    }
    return num4 * factorial4(num4 - 1);
}
console.log(factorial4(-7)); 


